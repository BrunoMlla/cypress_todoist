describe('Login do sistema ', () => {
    before(() => {
      cy.visit('https://todoist.com/')
    })

    it('Cenário 1 - Login Inválido', () => {
        cy.log('DADO que estou na tela de Login E Digite o login e a senha incorretos ' +  
                'QUANDO eu clicar no botão logar ENTAO não logarei no sistema ')
        cy.LoginSistema('bbrunomlla@gmail.com', '123')

        cy.FalhaLogin('Wrong email or password.')
      })

    it('Cenário 2 - Login Inválido - Senha em branco', () => {
        cy.log('DADO que estou na tela de Login E Digite o login e a senha em branco ' +
                'QUANDO eu clicar no botão logar ENTAO não logarei no sistema ')
        cy.LoginSistema('bbrunomlla@gmail.com', '')
        cy.FalhaLogin('Blank password')
    })

    it('Cenário 3 - Login Válido', () => {
        cy.log('DADO que estou na tela de Login E Digite o login e a senha ' + 
                'QUANDO eu clicar no botão logar ENTAO logarei no sistema ')
        cy.LoginSistema('bbrunomlla@gmail.com', 'Teste001')
        cy.title().should('contain', 'Today: Todoist')
    })
  })

  describe('CRUD do sistema ', () => {
    it('Cenário 1 - Adicionar Tarefa', () => {
        cy.log('DADO que estou na tela de tarefas ' + 
                'QUANDO eu clicar no botão de Adicionar Tarefa ENTAO aparecerá a tarefa na lista ')

        cy.AdicionarTarefa('Tarefa 001')
        cy.get('.section_day').contains('Tarefa 001')
    })

    it('Cenário 2 - Não Adicionar Tarefa', () => {
        cy.log('DADO que estou na tela de tarefas ' + 
                'QUANDO eu clicar no botão de Adicionar Tarefa e clico em cancelar ENTAO não aparecerá a tarefa na lista ')
        cy.get('ul li.agenda_add_task').click()
        cy.get('.DraftEditor-root').type('Tarefa 002')
        cy.get('.cancel').click()
        cy.get('.section_day').contains('Tarefa 002').should('not.exist')
    })

    it('Cenário 3 - Editar Tarefa', () => {
        cy.log('DADO que estou na tela de tarefas ' +
                'QUANDO eu clicar no botão de Editar Tarefa ENTAO alterará a tarefa na lista ')

        cy.get('.section_day').contains('Tarefa 001').click()
        cy.get('.task_content').click()
        cy.get('.DraftEditor-root').type(' Editada')
        cy.get('.item_editor_submit').click()
        cy.get('.item_detail_close').click()
        cy.get('.section_day').contains('Tarefa 001 Editada')
    })

    it('Cenário 4 - Não Editar Tarefa', () => {
        cy.log('DADO que estou na tela de tarefas ' + 
                'QUANDO eu clicar no botão de Editar Tarefa e clico em cancelar ENTAO não alterará a tarefa na lista ')

        cy.get('.section_day').contains('Tarefa 001 Editada').click()
        cy.get('.task_content').click()
        cy.get('.cancel').click()
        cy.get('.item_detail_close').click()
        cy.get('.section_day').contains('Tarefa 001 Editada')
    })

    it('Cenário 5 - Excluir Tarefa', () => {
        cy.log('DADO que estou na tela de tarefas ' + 
                'QUANDO eu clicar no botão de Excluir Tarefa ENTAO a tarefa será removida da lista ')

        cy.get('.section_day').contains('Tarefa 001 Editada').click()
        cy.get('.item_actions_more').click()
        cy.get('.danger_menu').contains('Delete task').click()
        cy.get('.ist_button_red').contains('Delete').click()
        cy.get('.section_day').contains('Tarefa 001 Editada').should('not.exist');
    })

    it('Cenário 6 - Concluir Tarefa', () => {
        cy.log('DADO que estou na tela de tarefas ' + 
                'QUANDO eu clicar no botão de Concluir Tarefa ENTAO a tarefa será removida da lista ')

        cy.AdicionarTarefa('Nova Tarefa 001')
        cy.get('.ist_checkbox').click()
        cy.get('.section_day').contains('Nova Tarefa 001').should('not.exist');
    })
  })

  describe('Operações da tela de tarefas ', () => {
    it('Cenário 1 - Filtrar Tarefa', () => {
        cy.AdicionarTarefa('Nova Tarefa 001')
        cy.AdicionarTarefa('Nova Tarefa 002')

        cy.get('.quick_find__input').type('Nova Tarefa 001{enter}')
        cy.get('.active').contains('Nova Tarefa 001')
        cy.get('.active').contains('Nova Tarefa 002').should('not.exist');
    })
  })

//   describe('Logout do sistema', () => {
//     it('Cenário 4 - Logout Válido', () => {
//         cy.log('DADO que estou na tela de tarefas QUANDO eu clicar no botão de ferramentas e no botão de deslogar ENTAO deslogarei do sistema ')
//         cy.LogoutSistema()
//     })
//   })